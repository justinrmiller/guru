import com.github.retronym.SbtOneJar._

oneJarSettings

name := "guru"

version := "0.1"

scalaVersion := "2.10.5"

resolvers ++= Seq(
  "Twitter repository" at "http://maven.twttr.com",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases/"
)

libraryDependencies ++= Seq(
  "com.twitter.finatra" %% "finatra-http"   % "2.1.0",
  "com.typesafe"        % "config"          % "1.3.0",
  "ch.qos.logback"      % "logback-classic" % "1.1.3",
  "org.reflections"     % "reflections"     % "0.9.10",
  "com.google.guava"    % "guava"           % "19.0",
  "com.livestream"      %% "scredis"        % "2.0.6"
)
