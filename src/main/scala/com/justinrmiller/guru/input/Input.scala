package com.justinrmiller.guru.input

/**
 * @author justinmiller
 */
sealed trait Input

case class DoubleInput(value: Double) extends Input

case class StringInput(value: String) extends Input

case class BooleanInput(value: Boolean) extends Input

case class LongInput(value: Long) extends Input

case class BigDecimalInput(value: BigDecimal) extends Input

case class ByteArrayInput(value: Array[Byte]) extends Input

// Note, this class will abandon type safety on "value" if used with validateNameAndType
case class AnyInput(value: Any) extends Input
