package com.justinrmiller.guru.demos

import com.justinrmiller.guru.input.{AnyInput, LongInput}
import guru.models.FizzBuzzModel
import com.justinrmiller.guru.models.{ScoreError, ScoreSuccess}

import scala.collection.immutable.HashMap

/**
 * Created by Justin R. Miller on 12/10/15.
 */
object DemoFizzBuzz {
  def input(input: Long) = {
    HashMap(
      "input" -> LongInput(input),
      "testany" -> AnyInput(HashMap("1" -> "2"))   // extra input won't cause the compute method to blow up on it's own
    )
  }

  def main(args: Array[String]) {
    val fizzBuzzModel = new FizzBuzzModel

    println(s"Evaluating validation ${fizzBuzzModel.getClass.getSimpleName} with good input")
    fizzBuzzModel.compute(input(1)) match {
      case success: ScoreSuccess => println(success)
      case error: ScoreError => println(error)
      case _ => println("Uhh... oops? :)")
    }

    println(s"Evaluating ${fizzBuzzModel.getClass.getSimpleName} with good input")
    1 to 10 foreach { x =>
      println(fizzBuzzModel.compute(input(x)))
    }

    val numFizzBuzzes = 1000000
    println(s"\nComputing fizzbuzz from 1 to ${numFizzBuzzes}")

    val startTimeInMillis = System.currentTimeMillis
    1 to numFizzBuzzes foreach { x => fizzBuzzModel.compute(input(x)) }
    println(s"\nTime Taken in Millis: ${System.currentTimeMillis - startTimeInMillis}")
  }
}
