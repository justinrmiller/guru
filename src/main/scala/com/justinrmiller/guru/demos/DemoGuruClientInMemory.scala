package com.justinrmiller.guru.demos

import com.justinrmiller.guru.client.{InMemoryGuruClient, GuruClient}
import com.justinrmiller.guru.input.LongInput
import guru.models.FizzBuzzModel

import scala.collection.immutable.HashMap

/**
 * Created by Justin R. Miller on 1/13/16.
 */
object DemoGuruClientInMemory {
  def main(args: Array[String]) {
    val client: GuruClient = InMemoryGuruClient(Map("guru:model:FizzBuzzModel" -> new FizzBuzzModel))

    for (i <- 1 to 1000) {
      val scoreResult = client.scoreModel("FizzBuzzModel", HashMap("input" -> LongInput(i)))
      println(scoreResult)
    }

    client.shutdown()
  }
}