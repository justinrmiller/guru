package com.justinrmiller.guru.demos

import com.justinrmiller.guru.input.{StringInput, LongInput}
import guru.models.TrafficCopModel

import scala.collection.immutable.HashMap

/**
 * Created by Justin R. Miller on 12/10/15.
 */
object DemoTrafficCop {
  def main(args: Array[String]): Unit = {
    val trafficCopModel = new TrafficCopModel

    val inputs = HashMap(
      "srcBytes" -> LongInput(10000001),
      "dstBytes" -> LongInput(20000),
      "protocol" -> StringInput("HTTP")
    )

    println(s"\nCalculating whether or not a particular period is suspicious:")
    println(trafficCopModel.compute(inputs))

    val numTrafficCop = 1000000
    println(s"\nComputing suspicious traffic $numTrafficCop times")

    val startTimeInMillisTraffic = System.currentTimeMillis
    1 to numTrafficCop foreach { x => trafficCopModel.compute(inputs) }
    println(s"Time Taken in Millis: ${System.currentTimeMillis - startTimeInMillisTraffic}")
  }
}
