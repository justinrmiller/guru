package com.justinrmiller.guru.demos

import com.justinrmiller.guru.input.StringInput
import guru.models.LevenshteinDistanceModel
import com.justinrmiller.guru.input.LongInput

import scala.collection.immutable.HashMap

/**
 * Created by Justin R. Miller on 12/10/15.
 */
object DemoLevenshteinDistance {
  def main(args: Array[String]) {
    val levenshteinDistanceModel = new LevenshteinDistanceModel

    val goodInput = HashMap(
      "input1" -> StringInput("bob"),
      "input2" -> StringInput("tom")
    )
    val badInput = HashMap(
      "input1" -> StringInput("bob"),
      "input2" -> LongInput(10)
    )

    println(s"Evaluating validation ${levenshteinDistanceModel.getClass.getSimpleName} with good input")
    println(levenshteinDistanceModel.compute(goodInput))

    println(s"\nEvaluating validation ${levenshteinDistanceModel.getClass.getSimpleName} with bad input")
    println(levenshteinDistanceModel.compute(badInput))

    println(s"\nCalculating levenshtein distance of bob and tom:")
    println(levenshteinDistanceModel.compute(goodInput))

    val numLevenstein = 1000000
    println(s"\nComputing levenshtein distance ${numLevenstein} times")

    val startTimeInMillisLeven = System.currentTimeMillis
    1 to numLevenstein foreach { x => levenshteinDistanceModel.compute(goodInput) }
    println(s"Time Taken in Millis: ${System.currentTimeMillis - startTimeInMillisLeven}")
  }
}
