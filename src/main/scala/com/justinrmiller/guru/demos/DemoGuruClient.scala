package com.justinrmiller.guru.demos

import akka.actor.ActorSystem
import scredis.Client

import scala.collection.immutable.HashMap

import com.justinrmiller.guru.client.{RedisGuruClient, GuruClient}
import com.justinrmiller.guru.input.LongInput

/**
 * @author justinmiller
 */
object DemoGuruClient {
  def main(args: Array[String]) {
    implicit val system = ActorSystem("guru-client-actor-system")

    val scredis = Client(host = "localhost", port = 6379, passwordOpt = None)
    val client: GuruClient = RedisGuruClient(scredis)

    for (i <- 1 to 1000) {
      val scoreResult = client.scoreModel("FizzBuzzModel", HashMap("input" -> LongInput(i)))
      println(scoreResult)
    }
    client.shutdown()
    system.shutdown()
  }
}
