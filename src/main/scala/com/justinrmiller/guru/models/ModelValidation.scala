package com.justinrmiller.guru.models

import com.justinrmiller.guru.input.Input

import scala.collection.mutable.ListBuffer

/**
 * @author justinmiller
 */
object ModelValidation {
  def validateNameAndType(inputDefinition: Map[String, Class[_ <: Input]], input: Map[String, Input]) = {
    val validations = ListBuffer[Validation]()

    inputDefinition.map { in =>
      val name = in._1
      val klass = in._2

      if (!input.contains(name)) {
        validations += MissingInformation(name)
      } else if (input.contains(name) && input(name).getClass != klass) {
        validations += IncorrectType(name)
      }
    }

    validations.toList
  }
}