package com.justinrmiller.guru.models

/**
 * @author justinmiller
 */
sealed trait Validation

case class MissingInformation(missingField: String) extends Validation
case class IncorrectType(erroneousType: String) extends Validation
