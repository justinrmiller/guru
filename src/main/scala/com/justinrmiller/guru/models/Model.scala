package com.justinrmiller.guru.models

import com.justinrmiller.guru.input.Input

/**
 * Created by Justin R. Miller on 12/10/15.
 */
trait Model extends java.io.Serializable {
  def inputs: Map[String, Class[_ <: Input]]

  def compute(input: Map[String, Input]) : ScoreResult
}
