package com.justinrmiller.guru.models

/**
 * @author justinmiller
 */

sealed trait Score

case class BooleanScore(score: Boolean) extends Score
case class LongScore(score: Long) extends Score
case class IntScore(score: Int) extends Score
case class StringScore(score: String) extends Score
case class BigDecimalScore(score: BigDecimal) extends Score
case class ByteArrayScore(score: Array[Byte]) extends Score

sealed trait ScoreResult

case class ScoreError(message: String, validationErrors: List[Validation] = List.empty) extends ScoreResult
case class ScoreSuccess(score: Score, subScores: Map[String, Score]) extends ScoreResult