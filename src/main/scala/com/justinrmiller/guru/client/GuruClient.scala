package com.justinrmiller.guru.client

import java.io.{ObjectOutputStream, ByteArrayOutputStream}
import java.util.{TimerTask, Timer}
import java.util.concurrent.TimeUnit

import scala.collection.concurrent.TrieMap
import scala.concurrent.duration.Duration
import scala.concurrent.Future

import com.justinrmiller.guru.input.Input
import com.justinrmiller.guru.models._

object GuruClient {
  // should this really go here?
  def serializeModel(modelClass: Class[_ <: Model]): Array[Byte] = {
    try {
      // load model
      val byteOut = new ByteArrayOutputStream()
      val out = new ObjectOutputStream(byteOut)
      val clazz = Class.forName(modelClass.getName)
      val model = clazz.newInstance()

      // serialize model
      out.writeObject(model)
      out.close()
      byteOut.close()

      println(s"Successfully Serialized (${byteOut.size} bytes): ${modelClass.getSimpleName}")

      byteOut.toByteArray
    } catch {
      case t: Throwable => {
        val error = s"Failed to serialize model: ${t.getMessage}"
        println(error)
        throw new RuntimeException(error)
      }
    }
  }
}

abstract class GuruClient() {
  def prefix = "guru:model"
  def cacheRefreshPeriod: Duration = Duration(1, TimeUnit.MINUTES)
  def modelKeyRetrievalTimeout: Duration = Duration(1, TimeUnit.MINUTES)
  def cacheModelRetrievalTimeout: Duration = Duration(1, TimeUnit.MINUTES)

  val modelCache = TrieMap[String, Model]()
  val cacheRefreshTimer = new Timer

  initialize(cacheRefreshPeriod)

  def shutdown() = {
    cacheRefreshTimer.cancel()
  }

  def getModel(modelName: String): Option[Model] = modelCache.get(s"$prefix:$modelName")
  def getModels: TrieMap[String, Model] = modelCache

  def scoreModel(modelName: String, inputs: Map[String, Input]) : ScoreResult = {
    try {
      modelCache
        .get(s"$prefix:$modelName")
        .map(z => z.compute(inputs))
        .getOrElse(ScoreError("Model does not exist."))
    } catch {
      case t: Throwable => {
        val error = s"Failed to deserialize model ${t.getMessage}"
        ScoreError(error)
      }
    }
  }

  def storeModel(name: String, modelClass: Class[_ <: Model]) : Future[Boolean]

  protected def retrieveModelsFromStore(): Map[String, Model]

  private def initialize(refreshPeriod : Duration) = {
    // initialize model cache
    retrieveModelsFromStore().foreach { modelEntry => modelCache.put(modelEntry._1, modelEntry._2) }

    // set timer to periodically refresh model cache
    cacheRefreshTimer.scheduleAtFixedRate(new TimerTask() {
      def run() = {
        retrieveModelsFromStore().foreach { modelEntry => modelCache.put(modelEntry._1, modelEntry._2) }
      }
    }, 0, refreshPeriod.toSeconds * 1000)
  }
}
