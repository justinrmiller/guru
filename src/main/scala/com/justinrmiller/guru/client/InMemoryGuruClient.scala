package com.justinrmiller.guru.client

import com.justinrmiller.guru.models.Model

import scala.concurrent.Future

/**
 * Created by Justin R. Miller on 1/13/16.
 */
class InMemoryGuruClient(models: Map[String, Model]) extends GuruClient {
  override def storeModel(name: String, modelClass: Class[_ <: Model]) : Future[Boolean] = {
    Future.successful(false)
  }

  override def retrieveModelsFromStore(): Map[String, Model] = {
    models
  }
}

object InMemoryGuruClient {
  def apply(models: Map[String, Model]) =
    new InMemoryGuruClient(models)
}
