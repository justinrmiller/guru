package com.justinrmiller.guru.client

import java.io.{ByteArrayInputStream, ObjectInputStream}

import akka.actor.ActorSystem
import com.google.common.io.BaseEncoding
import com.justinrmiller.guru.models.Model

import scala.concurrent.{Await, Future}

/**
 * Created by Justin R. Miller on 1/13/16.
 */
class RedisGuruClient(val client : scredis.Client) extends GuruClient {
  override def storeModel(name: String, modelClass: Class[_ <: Model]) : Future[Boolean] = {

    try {
      val byteOut = GuruClient.serializeModel(modelClass)

      println(s"Successfully Serialized (${byteOut.size} bytes): ${modelClass.getSimpleName}")

      // save to redis
      client.set(s"$prefix:$name", BaseEncoding.base32().encode(byteOut))

      Future.successful(true)
    } catch {
      case t: Throwable => {
        val error = s"Failed to serialize/store model named $name : ${t.getMessage}"
        println(error)
        Future.failed(t)
      }
    }
  }

  override def retrieveModelsFromStore(): Map[String, Model] = {
    val keys = Await.result(client.keys(s"$prefix:*"), modelKeyRetrievalTimeout)
    println(s"Caching $keys")

    val retrievedModels = collection.mutable.HashMap[String, Model]()

    if (keys.isEmpty) {
      println("No keys to cache!")
    } else {
      val startTime = System.currentTimeMillis()

      Await.result(client.mGetAsMap(keys.toSeq:_*), cacheModelRetrievalTimeout).foreach { result =>
        val key = result._1
        val value = result._2

        try {
          // deserialize model
          val in = new ObjectInputStream(new ByteArrayInputStream(BaseEncoding.base32().decode(value)))
          retrievedModels.put(key, in.readObject().asInstanceOf[Model])
          in.close()
        } catch {
          case _: Throwable =>
            println("Failed to deserialize model ${key}")
        }
      }

      println(s"Cache refresh took: ${System.currentTimeMillis - startTime} ms")
    }

    retrievedModels.toMap
  }
}

object RedisGuruClient {
  def apply(client: scredis.Client)(implicit system: ActorSystem) =
    new RedisGuruClient(client)
}