package com.justinrmiller.guru.utils.redis

import akka.actor.ActorSystem
import scredis.Client

import scala.collection.immutable.HashMap

import com.justinrmiller.guru.client.{RedisGuruClient, GuruClient}
import com.justinrmiller.guru.input.StringInput

/**
 * Created by Justin R. Miller on 12/10/15.
 */
object ModelRedisReader {
  def main(args: Array[String]) {
    implicit val system = ActorSystem("guru-client-actor-system")

    val scredis = Client(host = "localhost", port = 6379, passwordOpt = None)
    val client: GuruClient = RedisGuruClient(scredis)(system)

    try {
      client.getModel("LevenshteinDistanceModel").foreach { model =>
        println(s"Successfully deserialized model: ${model.getClass.getSimpleName}")

        println(s"\nCalculating levenshtein distance of bob and tom:")
        println(model.compute(HashMap("input1" -> StringInput("bob"), "input2" -> StringInput("tom"))))

        val numLevenstein = 1000000
        println(s"\nComputing levenshtein distance ${numLevenstein} times")

        val startTimeInMillisLeven = System.currentTimeMillis
        1 to numLevenstein foreach { x => model.compute(HashMap("input1" -> StringInput("bob"), "input2" -> StringInput("tom"))) }
        println(s"Time Taken in Millis: ${System.currentTimeMillis - startTimeInMillisLeven}")
      }

      client.shutdown()
      system.shutdown()
    } catch {
      case t: Throwable => {
        println(s"Failed to instantiate/use model ${t.getMessage}")
      }
    }
  }
}
