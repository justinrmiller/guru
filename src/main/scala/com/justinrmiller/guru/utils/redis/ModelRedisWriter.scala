package com.justinrmiller.guru.utils.redis

import akka.actor.ActorSystem
import org.reflections.Reflections
import scredis.Client

import scala.collection.JavaConverters._
import scala.concurrent.Future

import com.justinrmiller.guru.client.{RedisGuruClient, GuruClient}
import com.justinrmiller.guru.models.Model

/**
 * Created by Justin R. Miller on 12/10/15.
 */
object ModelRedisWriter {
  def main(args: Array[String]) {
    // todo: should the naming be dynamically generated?
    implicit val system = ActorSystem("guru-client-actor-system")

    val scredis = Client(host = "localhost", port = 6379, passwordOpt = None)
    val client: GuruClient = RedisGuruClient(scredis)

    val modelClasses = new Reflections("guru.models").getSubTypesOf(classOf[Model]).asScala

    for (i <- 1 to 1) {
      modelClasses.map(modelClass => {
        try {
          client.storeModel(if (i != 1) modelClass.getSimpleName + i else modelClass.getSimpleName, modelClass)
        } catch {
          case t: Throwable =>
            println("Something messed up when trying to store ${modelClass.getSimpleName}")
            Future.failed(t)
        }
      })
    }

    client.shutdown()
    system.shutdown()
  }
}
