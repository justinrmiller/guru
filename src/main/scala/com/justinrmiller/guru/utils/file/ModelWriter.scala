package com.justinrmiller.guru.utils.file

import java.io.{FileOutputStream, ObjectOutputStream}

import com.justinrmiller.guru.models.Model
import org.reflections.Reflections

import scala.collection.JavaConverters._

/**
 * Created by Justin R. Miller on 12/10/15.
 */
object ModelWriter {
  def main(args: Array[String]) {
    val reflections = new Reflections("guru.models")

    val modelClasses = reflections.getSubTypesOf(classOf[Model]).asScala

    // write models to disk
    modelClasses.foreach(modelClass => {
      try {
        val fileOut = new FileOutputStream(s"/tmp/${modelClass.getSimpleName}.model")
        val out = new ObjectOutputStream(fileOut)
        val clazz = Class.forName(modelClass.getName)
        val model = clazz.newInstance()
        out.writeObject(model)
        out.close()
        fileOut.close()
        println(s"Successfully Serialized: ${modelClass.getSimpleName} as ${modelClass.getSimpleName}.model")
      } catch {
        case e: Exception => println("Failed to instantiate/serialize ${modelClass.getSimpleName}")
      }
    })
  }
}
