package com.justinrmiller.guru.server

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import com.twitter.finagle.httpx.Request
import com.twitter.finatra.http.routing.HttpRouter
import com.twitter.finatra.http.{Controller, HttpServer}

import com.justinrmiller.guru.client.{RedisGuruClient, GuruClient}
import scredis.Client

import scala.concurrent.duration.Duration

object GuruMain extends GuruServer

case class ModelRequest(name: String)

class ModelController(client: GuruClient) extends Controller {
  //  "Hello " + request.params.getOrElse("name", "unnamed")

  get("/model") { request: Request =>
    info("Listing models")
    val models = client.getModels.map(x => Map("name" -> x._1, "inputs" -> x._2.inputs)).toList
    models
  }

  post("/model") { modelRequest: ModelRequest =>
    "Uploading Model " + modelRequest.name
  }
}

class GuruServer extends HttpServer {
  override def modules = Seq()

  override def configureHttp(router: HttpRouter) {
    implicit val system = ActorSystem("guru-client-actor-system")

    val scredis = Client(host = "localhost", port = 6379, passwordOpt = None)
    val client: GuruClient = RedisGuruClient(scredis)

    val mc = new ModelController(client)
    router.add(mc)
  }
}