package guru.models

import com.justinrmiller.guru.input.{StringInput, Input}
import com.justinrmiller.guru.models._

import scala.math._

/**
 * Created by Justin R. Miller on 12/10/15.
 */
object LevenshteinDistanceModel {
  def minimum(i1: Int, i2: Int, i3: Int) = min(min(i1, i2), i3)

  def distance(s1:String, s2:String) = {
    val dist = Array.tabulate(s2.length+1, s1.length+1) { (j,i)=> if( j==0) i else if (i==0) j else 0 }

    for (j<-1 to s2.length; i<-1 to s1.length) {
      dist(j)(i) = if (s2(j - 1) == s1(i - 1)) {
        dist(j - 1)(i - 1)
      } else {
        minimum(dist(j - 1)(i) + 1, dist(j)(i - 1) + 1, dist(j - 1)(i - 1) + 1)
      }
    }

    dist(s2.length)(s1.length)
  }
}

class LevenshteinDistanceModel extends Model {
  override def inputs: Map[String, Class[_ <: Input]] = Map(
    "input1" -> classOf[StringInput],
    "input2" -> classOf[StringInput]
  )

  override def compute(input: Map[String, Input]) : ScoreResult = {
    val validations = ModelValidation.validateNameAndType(inputs, input)

    if (validations.nonEmpty) {
      ScoreError(message = "Failed validations", validationErrors = validations)
    } else {
      val input1AsString = input("input1").asInstanceOf[StringInput].value
      val input2AsString = input("input2").asInstanceOf[StringInput].value

      ScoreSuccess(IntScore(LevenshteinDistanceModel.distance(input1AsString, input2AsString)), Map.empty)
    }
  }
}
