package guru.models

import com.justinrmiller.guru.input.{LongInput, StringInput, Input}
import com.justinrmiller.guru.models._

import scala.collection.immutable.HashMap

/**
 * Created by Justin R. Miller on 12/10/15.
 */
class TrafficCopModel extends Model {
  val srcBytesThreshold = HashMap("SSH" -> 40000, "HTTP" -> 1000000)
  val dstBytesThreshold = HashMap("SSH" -> 60000, "HTTP" -> 32000)

  override def inputs: Map[String, Class[_ <: Input]] = Map(
    "srcBytes" -> classOf[LongInput],
    "dstBytes" -> classOf[LongInput],
    "protocol" -> classOf[StringInput]
  )

  override def compute(input: Map[String, Input]) : ScoreResult = {
    val validations = ModelValidation.validateNameAndType(inputs, input)

    if (validations.nonEmpty) {
      ScoreError(message = "Failed validations", validationErrors = validations)
    } else {
      val srcBytesAsInt = input("srcBytes").asInstanceOf[LongInput].value
      val dstBytesAsInt = input("dstBytes").asInstanceOf[LongInput].value
      val protocolAsString = input("protocol").asInstanceOf[StringInput].value

      val score = (
        srcBytesAsInt > srcBytesThreshold(protocolAsString),
        dstBytesAsInt > dstBytesThreshold(protocolAsString)
        ) match {
        case (true, true) => true
        case (false, true) => false
        case (true, false) => true
        case (false, false) => false
      }

      val srcBytesSubscore = srcBytesAsInt > srcBytesThreshold(protocolAsString)
      val dstBytesSubscore = dstBytesAsInt > dstBytesThreshold(protocolAsString)

      val subScores = Map[String, Score](
        "srcBytes" -> BooleanScore(srcBytesSubscore),
        "dstBytes" -> BooleanScore(dstBytesSubscore)
      )

      ScoreSuccess(BooleanScore(score), subScores)
    }
  }
}
