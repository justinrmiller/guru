package guru.models

import com.justinrmiller.guru.input.{AnyInput, LongInput, Input}
import com.justinrmiller.guru.models._


/**
 * Created by Justin R. Miller on 12/10/15.
 */
class FizzBuzzModel extends Model {
  override def inputs: Map[String, Class[_ <: Input]] = Map(
    "input" -> classOf[LongInput]
  )

  override def compute(input: Map[String, Input]) : ScoreResult = {
    val validations = ModelValidation.validateNameAndType(inputs, input)

    if (validations.nonEmpty) {
      ScoreError(message = "Failed validations", validationErrors = validations)
    } else {

      val inputAsInt = input("input").asInstanceOf[LongInput].value

      val score = (inputAsInt % 3, inputAsInt % 5, inputAsInt % 31) match {
        case (0, 0, _) => "FizzBuzz"
        case (0, _, _) => "Fizz"
        case (_, 0, _) => "Buzz"
        case (_, _, 0) => "Taz"
        case _ => inputAsInt.toString
      }

      ScoreSuccess(StringScore(score), Map.empty)
    }
  }
}
